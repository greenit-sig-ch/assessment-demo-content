/***********************************************************************************************************************
 * Custom handlers package.
 * 
 * 1. Define new function called when the user has given an answer
 * to a singleChoice or multiChoice question:
 * 
 *		window.questionnaireFunctions.functionName = function (questionnaire, page, question, option) {
 *			...
 *			...
 *			...
 *		}
 * 
 * where:
 * - questionnaire - the model our questionnaire
 * - page - current page
 * - question - current question
 * - option - current option
 * 
 * ---------------------------------------------------------------------------------------------------------------------
 * 
 * 2. Function called when the user navigates to a new page,
 * before the page is rendered:
 * 
 *		window.questionnaireFunctions.handlerOnPageLoad = function (questionnaire, currentPage, newPage) {
 *			...
 *			...
 *			...
 *		}
 *		
 * where:
 * - questionnaire - the model our questionnaire
 * - currentPage - current page
 * - newPage - possible new page
 * 
 * ---------------------------------------------------------------------------------------------------------------------
 * 
 * 3. 
 * 
 */

window.questionnaireFunctions = {};

/**
 * Handler 
 * @param {Questionnaire} questionnaire
 * @param {Page} currentPage Current pag.
 * @param {Page} newPage New page.
 * @returns {void}
 */
window.questionnaireFunctions.handlerOnPageLoad = function (questionnaire, currentPage, newPage) {
	console.log('onPageLoad: page '
			+ (currentPage !== undefined ? currentPage.id : null)
			+ ' will change to '
			+ newPage.id);
};

/**
 * Example 1
 * @param {Questionnaire} questionnaire
 * @param {Page} page
 * @param {Question} question
 * @param {Option} option
 * @returns {void}
 */
window.questionnaireFunctions.function1 = function (questionnaire, page, question, option) {
	var result = 0;
	result |= question.answer.selection.get('yes') << 2;
	result |= question.answer.selection.get('no') << 1;
	result |= question.answer.selection.get('dnk');

	switch (result) {
		case 0:
			question.feedbackId = '';
			break;
		case 1:
			question.feedbackId = 'Feedback #1';
			break;
		case 2:
			question.feedbackId = 'Feedback #2';
			break;
		case 3:
			question.feedbackId = 'Feedback #3';
			break;
		case 4:
			question.feedbackId = 'Feedback #4';
			break;
		case 5:
			question.feedbackId = 'Feedback #5';
			break;
		case 6:
			question.feedbackId = 'Feedback #6';
			break;
		case 7:
			question.feedbackId = 'Feedback #7';
			break;
	}
};

/**
 * Example 2
 * @param {Questionnaire} questionnaire
 * @param {Page} page
 * @param {Question} question
 * @param {Option} option
 * @returns {void}
 */
window.questionnaireFunctions.function2 = function (questionnaire, page, question, option) {
	// parse answers
	if (question.answer.selection.get('yes')) {
		question.feedbackId = 'You say YES!';
	} else if (question.answer.selection.get('no')) {
		question.feedbackId = 'You say NO!';
	} else if (question.answer.selection.get('dnk')) {
		question.feedbackId = 'You say DNK!';
	}
};
